import 'package:flutter/material.dart';

class penyimpanan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      width: 180,
      decoration: BoxDecoration(
          color: Colors.amberAccent, borderRadius: BorderRadius.circular(10.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Icon(Icons.sd_card, color: Colors.blue[600], size: 34),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Penyimpanan', style: TextStyle(fontSize: 16)),
              Text('22/64 GB',
                  style: TextStyle(fontSize: 15, color: Colors.red)),
            ],
          )
        ],
      ),
    );
  }
}
