import 'package:flutter/material.dart';

class icon_awal extends StatelessWidget {
  final String title;
  final Color color;
  final IconData icon;

  const icon_awal(
      {required this.title, required this.color, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(this.icon, size: 48, color: this.color),
        Text(this.title,
            style: TextStyle(
                fontSize: 17, color: Colors.blue, fontWeight: FontWeight.bold)),
        SizedBox(height: 5),
        Text('8 item', style: TextStyle(fontSize: 14, color: Colors.white)),
      ],
    );
  }
}
