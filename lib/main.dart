import 'package:flutter/material.dart';

import 'package:menjajal/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prisma File Manager',
      initialRoute: 'home',
      routes: {'home': (_) => HomePage()},
    );
  }
}
