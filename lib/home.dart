import 'package:flutter/material.dart';
import 'penyimpanan.dart';
import 'icon_awal.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.redAccent,
          leading:
              Icon(Icons.format_list_bulleted, color: Colors.white, size: 28),
          elevation: 0.0,
          actions: [
            Icon(Icons.search, color: Colors.white, size: 28),
            SizedBox(width: 15),
            Icon(Icons.settings, color: Colors.white, size: 28),
            SizedBox(width: 20),
          ],
        ),
        body: Stack(
          children: [
            ListView(
              physics: BouncingScrollPhysics(),
              children: [
                SizedBox(height: 35),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Penyimpanan',
                          style: TextStyle(color: Colors.white, fontSize: 17)),
                    ],
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  height: 75,
                  width: double.infinity,
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: [
                      penyimpanan(),
                      penyimpanan(),
                    ],
                  ),
                ),
                SizedBox(height: 45),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Aplikasi',
                          style: TextStyle(color: Colors.white, fontSize: 17)),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    icon_awal(
                        title: 'Video',
                        color: Color(0xffFA8C5F),
                        icon: Icons.play_circle_outline),
                    icon_awal(
                        title: 'Gambar',
                        color: Color(0xff719Ef7),
                        icon: Icons.photo_album_outlined),
                    icon_awal(
                        title: 'Dokumen',
                        color: Color(0xfff67e86),
                        icon: Icons.sticky_note_2_outlined),
                    icon_awal(
                        title: 'Zip',
                        color: Color(0xff6ED5D5),
                        icon: Icons.ballot_outlined),
                  ],
                ),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    icon_awal(
                        title: 'APK',
                        color: Color(0xff6ed5d5),
                        icon: Icons.adb_outlined),
                    icon_awal(
                        title: 'Musik',
                        color: Color(0xffFA8C5F),
                        icon: Icons.music_note_outlined),
                    icon_awal(
                        title: 'Tag',
                        color: Color(0xff719Ef7),
                        icon: Icons.local_offer_outlined),
                    icon_awal(
                        title: 'File',
                        color: Color(0xfff67e86),
                        icon: Icons.adjust),
                  ],
                ),
                SizedBox(height: 45),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Baru saja diakses',
                          style: TextStyle(color: Colors.white, fontSize: 17)),
                      Row(
                        children: [
                          Icon(Icons.refresh, color: Colors.white, size: 27),
                          SizedBox(width: 10),
                          Icon(Icons.visibility_outlined,
                              color: Colors.white, size: 27),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.music_note,
                            color: Colors.blueAccent,
                          ),
                          Text('Spirit Carries On.mp3',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ],
                      ),
                      Text('2 jam yang lalu',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.adb_rounded,
                            color: Colors.blueAccent,
                          ),
                          Text('facebook.apk',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ],
                      ),
                      Text('3 jam yang lalu',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.image_rounded,
                            color: Colors.blueAccent,
                          ),
                          Text('Di pantai.jpg',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ],
                      ),
                      Text('5 jam yang lalu',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.picture_as_pdf,
                            color: Colors.blueAccent,
                          ),
                          Text('Pemberitahuan.pdf',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ],
                      ),
                      Text('1 hari yang lalu',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.play_circle,
                            color: Colors.blueAccent,
                          ),
                          Text('how to make cheese.mp4',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ],
                      ),
                      Text('3 hari yang lalu',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.file_copy,
                            color: Colors.blueAccent,
                          ),
                          Text('Laporan penjualan.pdf',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14)),
                        ],
                      ),
                      Text('3 hari yang lalu',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ));
  }
}
